const express = require('express'),
bodyParser = require('body-parser'),
cors = require('cors');

const incidentesRoute = require('./MRI/app');
const leccionesRoute = require('./MLA/app');
const gestionIncidentesRoute = require('./MGI/app');

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use('/incidentes', incidentesRoute); // Incident Reporting API Path
app.use('/lecciones', leccionesRoute); // Lesson API Path
app.use('/gestion-incidentes', gestionIncidentesRoute); // Incident Management API Path

var port = process.env.PORT || 3000;

app.get('/', function(req, res) {
    res.json({"SGI": "API"});
});

const server = app.listen(port, function(){
    console.log('Server running on port %d', port);
});
